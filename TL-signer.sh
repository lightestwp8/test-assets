rm -fr *.pem *.der *.jks *.p12 *.srl v3.ext *.pfx
alias ktpfx='keytool -storetype pkcs12 -keystore tlso.pfx'
alias ktpfxp='ktpfx -storepass 123456'
alias ktpfxpkp='ktpfxp -keypass 123456'



function genKP(){
  echo "Generate keypair for $1"
  ktpfxpkp -genkeypair -alias $1 -keyalg RSA -keysize 2048 -sigalg SHA512withRSA -validity 3650 \
        -dname "C=EU, ST=ST, L=ISTANBUL, O=LIGHTest Trust List Provider, OU=EID, CN=$1"
}


#export the keypair (private + cert) of the given alias ($1)
function exportKP(){
   echo "Export the $1 key and cert"
   ktpfxp -exportcert -alias $1 -file $1.cert.der
   
   echo "Convert the $1 cert DER to PEM"
   openssl x509 -inform DER -in $1.cert.der -outform PEM -out $1.cert.pem
   

   echo "Export the $1 private key step 1"
   keytool -importkeystore -srckeystore tlso.pfx  -srcstoretype PKCS12 -srcalias $1 \
        -destkeystore $1.p12 -deststoretype PKCS12 \
        -deststorepass 123456 -destkeypass 123456 -srckeypass 123456 -srcstorepass 123456
        
   echo "Export the $1 private key step 2"
   openssl pkcs12 -in $1.p12 -nodes -nocerts -out $1.key.pem -password pass:123456

   echo "Export the $1 private key step 3"
   openssl pkcs8 -topk8 -in $1.key.pem -inform PEM -outform DER -out $1.key.der -nocrypt

}

#generate a CSR for the given alias ($1)
function genCSR(){
  echo "Create a certificate request for $1"
  ktpfxp -certreq -alias $1 -file $1.req.pem
}


#sign the CSR of $1 with the key of $2, 
# $1: the alias to be signed
# $2: subject alt name
# $3: the CA alias that signs
# $4: TRUE if the new cert is CA, FALSE otherwise
function signCSR(){
   echo "authorityKeyIdentifier=keyid,issuer
   basicConstraints=CA:$4,pathlen:2
   keyUsage=digitalSignature, nonRepudiation
   subjectAltName=URI:$2
   issuerAltName=issuer:copy
   subjectKeyIdentifier=hash
   extendedKeyUsage=0.4.0.2231.3.0
   " > $1_v3.ext
   
   
echo "Sign the $1 certificate request with $2 "
openssl x509 -req -days 2500 -in $1.req.pem -inform PEM  \
        -CAkey $3.key.pem -CA $3.cert.pem -set_serial 12345678 \
        -out $1.cert.pem -outform PEM -sha512 -extfile $1_v3.ext
}

#convert a cert from one format to another
# $1: Alias
# $2: source format
# $3: dest format

function certConvert(){
  openssl x509 -inform $2 -outform $3 -in $1.cert.$2 -out $1.cert.$3
}


genKP TLSO

exportKP TLSO

genCSR TLSO

signCSR TLSO  tlso.eu   TLSO   FALSE


echo "Convert the new certificates to der"
certConvert TLSO pem der

ktpfxp -list

echo "Import the new certificate"
ktpfxp -importcert -alias TLSO -file tlso.cert.der -noprompt

echo "List objects"
ktpfxp -list

cp tlso.pfx ../

echo DONE
